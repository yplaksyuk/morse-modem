/**
 * @author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * @date Dec 28, 2017
 */

#ifndef BUFFER_H
#define BUFFER_H

#include <stdint.h>

struct buffer_t
{
	int8_t *data; // pointer to buffer's data array
	int8_t  mask; // index mask
	int8_t  head; // head index
	int8_t  tail; // tail index
};

/**
 * Initializes buffer structure.
 * 
 * @param buf pointer to buffer_t structure being initialized
 * @param data pointer to pre-allocated data array
 * @param size data array size, must be power of 2
 */
void buffer_init(struct buffer_t *buf, int8_t *data, uint8_t size);

/**
 * Reads non-zero byte from the buffer.
 * 
 * @param buf pointer to buffer_t structure
 * @return byte value or zero of buffer is empty
 */
int8_t buffer_get(struct buffer_t *buf);

/**
 * Puts non-zero byte value into the buffer.
 * If buffer is full, the character is ignored.
 * 
 * @param buf pointer to buffer_t structure
 * @param val value
 */
void buffer_put(struct buffer_t *buf, int8_t val);

#endif
