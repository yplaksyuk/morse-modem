/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Dec 11, 2019
 */
#include "event.h"

#include <avr/interrupt.h>

#define EVENT_QUEUE_SIZE 64


static event_t event_queue[EVENT_QUEUE_SIZE];
static int8_t event_p, event_q;

void event_put(int8_t type, int8_t param)
{
	uint8_t sreg = SREG; cli();

	if (event_queue[event_p].type == EVENT_NONE)
	{
		event_queue[event_p].type = type;
		event_queue[event_p].param = param;
		event_p = (event_p + 1) % EVENT_QUEUE_SIZE;
	}

	SREG = sreg;
}

event_t event_get()
{
	event_t e = event_queue[event_q];
	if (e.type != EVENT_NONE)
	{
		event_queue[event_q].type = EVENT_NONE;
		event_q = (event_q + 1) % EVENT_QUEUE_SIZE;
	}
	return e;
}

