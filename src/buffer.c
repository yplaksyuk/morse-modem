/**
 * @author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * @date Dec 28, 2017
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#include "buffer.h"

void buffer_init(struct buffer_t *buf, int8_t *data, uint8_t size)
{
	buf->data = data;
	buf->mask = size - 1;
	buf->head = buf->tail = 0;
}

int8_t buffer_get(struct buffer_t *buf)
{
	int8_t val = 0;

	uint8_t sreg = SREG;
	cli();

	if (buf->head != buf->tail)
	{
		val = buf->data[buf->head];
		buf->head = (buf->head + 1) & buf->mask;
	}

	SREG = sreg;
	return val;
}

void buffer_put(struct buffer_t *buf, int8_t val)
{
	if (val)
	{
		uint8_t sreg = SREG;
		cli();

		int8_t tail = (buf->tail + 1) & buf->mask;
		if (tail != buf->head)
		{
			buf->data[buf->tail] = val;
			buf->tail = tail;
		}

		SREG = sreg;
	}
}
