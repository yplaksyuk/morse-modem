/**
 * @author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * @date Jan 14, 2018
 */

#ifndef MORSE_H
#define MORSE_H

#include <stdint.h>

void morse_decode(int8_t duration);

/**
 * Convert ASCII character to Morse code binary representation.
 * 
 * @param ch ASCII code
 * @return Morse code or zero if there is no mapping.
 */
int8_t morse_to(char ch);

/**
 * Convert Morse code binary representation to ASCII code.
 * 
 * @param code Morse code
 * @return ASCII character or zero if cannot resolve.
 */
char morse_from(int8_t code);

#endif
