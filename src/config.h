/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Jan 7, 2018
 */

#ifndef CONFIG_H
#define CONFIG_H

#define LED_DDR        DDRB
#define LED_PORT       PORTB
#define LED_PAD        PB0

#define TONE_DDR  DDRD
#define TONE_PORT PORTD
#define TONE_PIN  PIND

#define TONE_ON   PD2 /* INT0 */
#define TONE_IN   PD3 /* INT1 */
#define TONE_OUT  PD4

#define EVENT_USART 1
#define EVENT_TONE  2
#define EVENT_MORSE 3

#endif
