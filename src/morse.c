/**
 * @author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * @date Jan 14, 2018
 */

#include "morse.h"
#include "event.h"
#include "config.h"
#include "ditdah.h"
#include "usart.h"
#include "tone.h"

#include <avr/pgmspace.h>

struct morse_code
{
	int8_t code;
	char ch;
};

union morse_code_word
{
	struct morse_code m;
	uint16_t w;
};

static const PROGMEM struct morse_code codes[] = {
	{ 0b11001100, '?' }, // 0x3F - '?'  ..--..
	{ 0b11010010, '"' }, // 0x22 - '"'  .-..-.
	{ 0b11010101, '.' }, // 0x2E - '.'  .-.-.-
	{ 0b11011010, '@' }, // 0x40 - '@'  .--.-.
	{ 0b11011110, '\''}, // 0x27 - '''  .----.
	{ 0b11100000, '5' }, // 0x35 - '5'  .....
	{ 0b11100001, '4' }, // 0x34 - '4'  ....-
	{ 0b11100011, '3' }, // 0x33 - '3'  ...--
	{ 0b11100111, '2' }, // 0x32 - '2'  ..---
	{ 0b11101111, '1' }, // 0x31 - '1'  .----
	{ 0b11110000, 'H' }, // 0x48 - 'H'  ....
	{ 0b11110001, 'V' }, // 0x56 - 'V'  ...-
	{ 0b11110010, 'F' }, // 0x46 - 'F'  ..-.
	{ 0b11110100, 'L' }, // 0x4C - 'L'  .-..
	{ 0b11110110, 'P' }, // 0x50 - 'P'  .--.
	{ 0b11110111, 'J' }, // 0x4A - 'J'  .---
	{ 0b11111000, 'S' }, // 0x53 - 'S'  ...
	{ 0b11111001, 'U' }, // 0x55 - 'U'  ..-
	{ 0b11111010, 'R' }, // 0x52 - 'R'  .-.
	{ 0b11111011, 'W' }, // 0x57 - 'W'  .--
	{ 0b11111100, 'I' }, // 0x49 - 'I'  ..
	{ 0b11111101, 'A' }, // 0x41 - 'A'  .-
	{ 0b11111110, 'E' }, // 0x45 - 'E'  .
	{ 0b00000001, 'T' }, // 0x54 - 'T'  -
	{ 0b00000010, 'N' }, // 0x4E - 'N'  -.
	{ 0b00000011, 'M' }, // 0x4D - 'M'  --
	{ 0b00000100, 'D' }, // 0x44 - 'D'  -..
	{ 0b00000101, 'K' }, // 0x4B - 'K'  -.-
	{ 0b00000110, 'G' }, // 0x47 - 'G'  --.
	{ 0b00000111, 'O' }, // 0x4F - 'O'  ---
	{ 0b00001000, 'B' }, // 0x42 - 'B'  -...
	{ 0b00001001, 'X' }, // 0x58 - 'X'  -..-
	{ 0b00001010, 'C' }, // 0x43 - 'C'  -.-.
	{ 0b00001011, 'Y' }, // 0x59 - 'Y'  -.--
	{ 0b00001100, 'Z' }, // 0x5A - 'Z'  --..
	{ 0b00001101, 'Q' }, // 0x51 - 'Q'  --.-
	{ 0b00010000, '6' }, // 0x36 - '6'  -....
	{ 0b00010001, '=' }, // 0x3D - '='  -...-
	{ 0b00010010, '/' }, // 0x2F - '/'  -..-. 
	{ 0b00011000, '7' }, // 0x37 - '7'  --...
	{ 0b00011100, '8' }, // 0x38 - '8'  ---.. 
	{ 0b00011110, '9' }, // 0x39 - '9'  ----.
	{ 0b00011111, '0' }, // 0x30 - '0'  ----- 
	{ 0b00100001, '-' }, // 0x2D - '-'  -....-
	{ 0b00110011, ',' }, // 0x2C - ','  --..--
	{ 0b00111000, ':' }, // 0x3A - ':'  ---...
};


void morse_decode(int8_t duration)
{
	static int8_t code;

	if (duration > 0)
	{
		int8_t ditdah = duration < ditdah_barrier() ? 0 : 1;
		if (code == 0 && ditdah == 0)
			code = 0xFF;

		code = (code << 1) | ditdah;
	}
	else if (duration == TONE_SPACE)
	{
		event_put(EVENT_MORSE, ' ');
	}
	else
	{
		if (-duration > ditdah_barrier())
		{
			char ch = morse_from(code);
			if (ch)
				event_put(EVENT_MORSE, ch);
			else
				event_put(EVENT_MORSE, '_');

			code = 0;
		}
	}
}

int8_t morse_to(char ch)
{
	if (ch >= 'a' && ch <= 'z') ch -= 0x20;

	for (int8_t i = 0; i < sizeof(codes) / sizeof(struct morse_code); ++i)
	{
		union morse_code_word v;

		v.w = pgm_read_word_near(codes + i);
		if (v.m.ch == ch) return v.m.code;
	}

	return 0;
}

char morse_from(int8_t code)
{
	union morse_code_word v;

	int8_t l = 0, r = sizeof(codes) / sizeof(struct morse_code);
	while (l + 1 < r)
	{
		int8_t i = (l + r) / 2;

		v.w = pgm_read_word_near(codes + i);
		if (code < v.m.code)
			r = i;
		else
			l = i;
	}

	v.w = pgm_read_word_near(codes + l);
	if (code == v.m.code) return v.m.ch;

	return 0;
}
