/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Jan 7, 2018
 */

#ifndef TONE_H
#define TONE_H

#include <stdint.h>

#define TONE_MAX   (+INT8_MAX)
#define TONE_MIN   (-INT8_MAX)
#define TONE_SPACE   INT8_MIN

/*!
 * Initialize tone detection module.
 */
void tone_init();

/*!
 * Sets tone frequency in Hz.
 *
 * \param freq tone frequency
 */
void tone_set(int16_t freq);

/*!
 * Sets average dit duration.
 *
 * \param duration
 */
void tone_dit(int8_t duration);

#endif
