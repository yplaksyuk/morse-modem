/**
 * @author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * @date Dec 27, 2017
 */

#ifndef USART_H
#define USART_H

#include <stdint.h>

#define USART_BAUD_RATE(baud) (F_CPU / (16L * baud) - 1)

#define USART_BAUD_1200 USART_BAUD_RATE(1200)
#define USART_BAUD_9600 USART_BAUD_RATE(9600)

/**
 * Initializes USART.
 * 
 * @param baud_rate baud rate, USART_BAUD_nnnn value
 */
void usart_init(uint16_t baud_rate);

/**
 * Writes character to the USART.
 * 
 * @param ch character to write
 */
void usart_putc(char ch);

/**
 * Writes null-terminated chracter string to the USART.
 * 
 * @param str character string pointer
 */
void usart_puts(const char *str);

void usart_putd(int8_t v);

void usart_putd16(int16_t v);

#endif
