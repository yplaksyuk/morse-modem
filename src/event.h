/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Dec 11, 2019
 */
#ifndef EVENT_H

#include <stdint.h>


/*!
 * Special event type which indicates that there is no any event available.
 */
#define EVENT_NONE   0

typedef struct
{
	int8_t type;    /* event type */
	int8_t param;   /* event parameter */
} event_t;


void event_put(int8_t type, int8_t param);

event_t event_get();

#endif

