#include <stdint.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#include "event.h"
#include "usart.h"
#include "tone.h"
#include "morse.h"
#include "ditdah.h"
#include "config.h"

int main(void)
{
	tone_init();
	usart_init(USART_BAUD_9600);

	// configure CPU sleep mode
	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
	sei();

	usart_puts("\r\n\r\nHello morse!\r\n");
	while (1)
	{
		event_t e = event_get();
		if (e.type == 0) continue;

		//usart_putc('.');
		switch (e.type)
		{
			case EVENT_USART:
				usart_putc(e.param);
				break;

			case EVENT_TONE:
				//ditdah_tone(e.param); // maintain dit/dah durations
				//usart_putd(e.param); usart_putc(' ');
				morse_decode(e.param); // decode dit/dah
				break;

			case EVENT_MORSE:
				usart_putc(e.param);
				//usart_putc('\r'); usart_putc('\n');
				break;
		}
		// sleep_cpu();

		// if (val > 0)
		// {
		//	 usart_putd(val);
		// }
		// else if(val > -128)
		// {
		//	 usart_putc('-');
		//	 usart_putd(-val);
		// }
		
	}

	return 0;
}
