/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Jan 2, 2020
 */

#include "ditdah.h"
#include "tone.h"

#define DITDAH_SIZE 8

static int8_t ditdah_value;


void ditdah_tone(int8_t duration)
{
	static int8_t buf[DITDAH_SIZE];
	static int8_t idx;

	if (duration < 0)
		duration = -duration;

	buf[idx++] = duration;
	idx %= DITDAH_SIZE;

	int8_t value = INT8_MAX;
	for (int8_t i = 0; i < DITDAH_SIZE; ++i)
	{
		if (value > buf[i])
			value = buf[i];
	}

	if (ditdah_value != value)
	{
		ditdah_value = value;
		tone_dit(value);
	}
}

int8_t ditdah_barrier()
{
	return 16; //ditdah_value * 2;
}

