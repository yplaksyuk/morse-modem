/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Jan 2, 2020
 */
#ifndef DITDAH_H
#define DITDAH_H

#include <stdint.h>

void ditdah_tone(int8_t duration);

int8_t ditdah_barrier();

#endif

