/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \since Jan 7, 2018
 */

#include <string.h>

#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>

#include "config.h"
#include "tone.h"
#include "buffer.h"
#include "usart.h"
#include "event.h"

#define TONE_VALUE(freq) ((uint8_t) ((F_CPU / 8 / 16) / freq - 1))
#define POW2N(x)         (((int8_t) (x >> 8)) * ((int8_t) (x >> 8)))


#define TONE_STATUS_DETECTED 0x01
#define TONE_STATUS_COMPA    0x02
#define TONE_STATUS_COMPB    0x04
#define TONE_STATUS_OVF      0x08

volatile static uint8_t tone_status = TONE_STATUS_OVF;        // current status bits
volatile static int16_t tone_level = 2000;                    // tone detection level
volatile static uint8_t tone_adc_adj = 118;                   // voltage divider produces 1.36v, i.e. 136
volatile static int16_t tone_tmp = 0;                         // temporary duration


void tone_init()
{
	LED_DDR |= _BV(LED_PAD);                                  // configure LED_PIN for output

	TCCR1A = 0;                                               // set TIMER1 normal operation
	TCCR1B = _BV(CS12) | _BV(CS10);                           // set TIMER1 prescaler: T1 = 1024 / Fcpu = 128us
	TCCR2 = _BV(WGM21) | _BV(CS21);                           // set TIMER2 prescaler: T2 = 8 / Fcpu = 1us, CTC mode, AS2 is assumed to be 0
	TIMSK = _BV(TOIE1) | _BV(OCIE1A) | _BV(OCIE1B) | _BV(OCIE2); // enable interrupts: TIMER1 OVF, TIMER1 COMPA, TIMER1 COMPB and TIMER2 COMP

	ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(ADLAR);             // ADC: use Vref = 2.56, values are 8-bit left-adjusted, read ADCH only
	ADCSRA = _BV(ADEN) | _BV(ADSC) | _BV(ADFR) | _BV(ADPS0);  // ADC: start free-running mode with /32 precaler (Fadc = 250kHz)

	tone_set(500);
	tone_dit(8); // ~12WPM
}

void tone_set(int16_t freq)
{
	OCR2 = TONE_VALUE(freq);
}

void tone_dit(int8_t duration)
{
	uint16_t cnt = ((uint16_t) duration) << 6; // restore resolution (6 bits)
	OCR1A = cnt << 1; // dit/dah barrier
	OCR1B = cnt << 3; // word barrier
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static uint8_t tone_detect(uint8_t adcv)
{
	static const int8_t COS[16] = { 127, 117, 89, 48, 0, -49, -90, -118, -127, -118, -90, -49, 0, 48, 89, 117 };
	static const int8_t SIN[16] = { 0, 48, 89, 117, 127, 117, 89, 48, 0, -49, -90, -118, -127, -118, -90, -49 };

	static int16_t I[16];
	static int16_t Q[16];
	static int16_t M[16];

	static int16_t X, Y, Z;
	static int8_t N;

	static int16_t T, W;

	int8_t v = (int8_t)(adcv - tone_adc_adj);
	if (v > 31) v = 31;
	if (v < -31) v = -31;

	// It is assumed that ADC0 input is ±30mV, which translates to values within ±30, i.e. s5 bits.
	// When multiplied by COS()/SIN() and shifted by 1, it is s5 + s7 - 1 = s11 bits.
	// The SUM of 16 those values fits into s11 + 4 = s15 bits exactly.
	X -= I[N]; I[N] = (v * COS[N]) >> 1; X += I[N];
	Y -= Q[N]; Q[N] = (v * SIN[N]) >> 1; Y += Q[N];

	int16_t m = POW2N(X) + POW2N(Y);
	Z -= M[N]; M[N] = (m >> 4); Z += M[N];
	N = (N + 1) & (16 - 1);

	//if (W < Z) W = Z;
	//if (++T > 16 * 500 * 3) { usart_putd16(W); usart_putc(','); T = W = 0; }
	if (Z > tone_level)
	{
		return TONE_STATUS_DETECTED;
	}
	else
		return 0;
}

static void tone_process()
{
	uint8_t status = tone_detect(ADCH);

	if ((tone_status & TONE_STATUS_DETECTED) ^ status)
	{
		uint16_t cnt = TCNT1; TCNT1 = 0; // slice and reset TIMER1 counter

		if (status)
			LED_PORT |= _BV(LED_PAD);
		else
			LED_PORT &= ~_BV(LED_PAD);

		if ((tone_status & (TONE_STATUS_COMPA | TONE_STATUS_COMPB | TONE_STATUS_OVF)) == 0)
		{
			cnt = 1 + (cnt >> 6);
			//if (cnt >>= 6) // narrow resolution to 128us * 2^6 = 8ms (40WPM ~ 30ms)
			{
				int8_t duration = cnt < TONE_MAX ? cnt : TONE_MAX; // normalize duration

				if (status)
					event_put(EVENT_TONE, -duration);
				else
					event_put(EVENT_TONE, +duration);
			}
		}

		tone_status = status; // set new status, clear all flags
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ISR(TIMER1_COMPA_vect)
{
	if ((tone_status & (TONE_STATUS_COMPA | TONE_STATUS_DETECTED)) == 0)
	{
		tone_status |= TONE_STATUS_COMPA;
		event_put(EVENT_TONE, TONE_MIN);
	}
}

ISR(TIMER1_COMPB_vect)
{
	if ((tone_status & (TONE_STATUS_COMPB | TONE_STATUS_DETECTED)) == 0)
	{
		tone_status |= TONE_STATUS_COMPB;
		event_put(EVENT_TONE, TONE_SPACE);
	}
}

ISR(TIMER1_OVF_vect)
{
	if ((tone_status & (TONE_STATUS_COMPA | TONE_STATUS_COMPB | TONE_STATUS_OVF)) == 0)
	{
		if (tone_status & TONE_STATUS_DETECTED)
			event_put(EVENT_TONE, TONE_MAX);
		else
			event_put(EVENT_TONE, TONE_MIN);
	}

	tone_status |= TONE_STATUS_OVF;
}

ISR(TIMER2_COMP_vect)
{
	//LED_PORT ^= _BV(LED_PAD);
	tone_process();
}
