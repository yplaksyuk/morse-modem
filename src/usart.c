/*!
 * \author Yuri Plaksyuk <yuri.plaksyuk@gmail.com>
 * \date Dec 27, 2017
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "usart.h"
#include "event.h"
#include "config.h"

#define USART_BUFSIZE 128

static int8_t usart_buf[USART_BUFSIZE];
static int8_t usart_p, usart_q;

void usart_init(uint16_t baud_rate)
{
	/* Set baud rate */
	UBRRH = (baud_rate >> 8);
	UBRRL = (baud_rate);

	/* Enable interrupts, RX and TX */
	UCSRB = _BV(RXCIE) | _BV(UDRIE) | _BV(RXEN) | _BV(TXEN);

	/* Set frame format: 8-N-1 */
	UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);
}

void usart_putc(char ch)
{
	if (ch)
	{
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			if (UCSRA & _BV(UDRE))
			{
				UDR = ch;
			}
			else
			{
				/*while (usart_buf[usart_p])
				{
					NONATOMIC_BLOCK(NONATOMIC_FORCEOFF)
					{
						while (usart_buf[usart_p]) ; // loop while the buffer is full
					}
				}*/
				
				usart_buf[usart_p] = ch;
				usart_p = (usart_p + 1) % USART_BUFSIZE;
			}
		}
	}
}

void usart_puts(const char *str)
{
	while (*str)
		usart_putc(*str++);
}

void usart_putd(int8_t v)
{
	if (v < 0)
	{
		usart_putc('-');
		v = -v;
	}

	int8_t m = 100;
	while (v < m && m > 1)
		m /= 10;

	while (m)
	{
		usart_putc('0' + (v / m));
		v %= m;
		m /= 10;
	}
}

void usart_putd16(int16_t v)
{
	if (v < 0)
	{
		usart_putc('-');
		v = -v;
	}

	int16_t m = 10000;
	while (m > v && m > 1)
		m /= 10;

	while (m)
	{
		usart_putc('0' + (v / m));
		v %= m;
		m /= 10;
	}
}


ISR(USART_RXC_vect)
{
	event_put(EVENT_USART, UDR);
}

ISR(USART_UDRE_vect)
{
	char ch = usart_buf[usart_q];
	if (ch)
	{
		UDR = ch;

		usart_buf[usart_q] = 0;
		usart_q = (usart_q + 1) % USART_BUFSIZE;
	}
}
